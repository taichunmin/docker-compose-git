FROM docker:latest
LABEL maintainer="taichunmin@gmail.com"

RUN apk --no-cache add bash git openssh py2-pip && \
    pip install docker-compose
